
$('.select-block button').on('click', function(){
	var $this = $(this).closest('.select-block');
	if ( $this.hasClass('active') ){
		$this.removeClass('active');
	} else {
		$('.select-block').removeClass('active');
		$this.addClass('active');
	}
});
$('.is-input input').on('click', function(){
	var $this = $(this).closest('.select-block');
	if ( $this.hasClass('active') ){
		$this.removeClass('active');
	} else {
		$('.select-block').removeClass('active');
		$this.addClass('active');
	}
});

$('.select-block:not(.is-checkbox-mode) .select-block__list a').on('click', function(event){
	var $this = $(this),
		$wrapper = $this.closest('.select-block'),
		$value = $this.attr('data-val'),
		$html = $this.html(),
		$input = $wrapper.find('input');
	$wrapper.addClass('is-chiose');
	$wrapper.find('.select-block__value-view').empty().append($html);
	$input.val($value);
	$wrapper.removeClass('active');
	$input.trigger('selected');
	event.preventDefault();
});

$('.select-block.is-checkbox-mode .select-block__list a').on('click', function(event){
	event.preventDefault();
});

$(document).on('click', function(e){
	if( $(e.target).closest('.select-block').length === 0 ){
		$('.select-block').removeClass('active');
	}
});

$(document).on('change', '.select-block.is-checkbox-mode input[type="checkbox"]', function(e){
	selectBlockCheckboxModeChangeListener($(this));
});

$(document).on('click', '.remove-select-tag', function(e) {
	var th = $(this);
	selectBlockCheckboxModeChangeListener($('#' + th.attr('data-id')).prop('checked', false));
});

function selectBlockCheckboxModeChangeListener(th) {
	var wrap = th.closest('.select-block'),
		buttonView = wrap.find('.select-block__value-view'),
		tagsWrap = wrap.find('.select-block___tags ul'),
		values = [];
	tagsWrap.empty();
	wrap.find('input[type="checkbox"]:checked').each(function(){
		var input = $(this),
			text = input.closest('li').children('a').text(),
			li = document.createElement('li'),
			span = document.createElement('span'),
			button = document.createElement('button');
		span.innerHTML = text;
		button.setAttribute('type', 'button');
		button.setAttribute('data-id', input.attr('id'));
		button.className = 'remove-select-tag';
		button.innerHTML = '<svg width="1em" height="1em" class="icon icon-close "><use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-close"></use></svg>';
		li.appendChild(span);
		li.appendChild(button);
		tagsWrap.append($(li));
		values.push(text);
	});
	buttonView.find('span').text(values.join(', '));
	if (values.length === 0) {
		wrap.removeClass("is-chiose");
	} else {
		wrap.addClass("is-chiose");
		if (buttonView.innerWidth() <= buttonView.find('span').outerWidth()) {
			tagsWrap.show();
		} else {
			tagsWrap.hide();
		}
	}
}



$('#clearfilter').on('click', function(){
	$(this).closest('.c-filters__inner').find('.select-block').removeClass('is-chiose');
	$(this).closest('.c-filters__inner').find('.select-block__value-view').text('');
});
$('#clearfilter-arcive').on('click', function(){
	$(this).closest('.c-archive__filters').find('.select-block').removeClass('is-chiose');
	$(this).closest('.c-archive__filters').find('.select-block__value-view').text('');
});
$('#clearstatsfilter').on('click', function(){
	$(this).closest('.c-stats__table-filter').find('.select-block').removeClass('is-chiose');
	$(this).closest('.c-stats__table-filter').find('.select-block__value-view').text('');
	$(this).closest('.c-stats__table-filter').find('#domen_place2').text('');	
});

$('#clearstatsfilter').on('apply.daterangepicker', function(ev, picker) {
  picker.setStartDate(something);
  picker.setEndDate(something);
});

$('.c-table__filter-sort').on('click', function(){
	$('.c-table__filter-sort').not(this).removeClass('sort-up sort-down');
	var $this = $(this);
	if ( $this.hasClass('sort-up') ){
		$this.removeClass('sort-up');
		$this.addClass('sort-down');
	} else {
		$this.hasClass('sort-down');
		$this.removeClass('sort-down');
		$this.addClass('sort-up');
	}
});

