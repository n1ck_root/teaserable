if ($('.js-calculator').length) {
	var form = $('.c-add-block__form'),
		displayFlex = ' display: -webkit-box; display: -moz-box; display: -ms-flexbox; display: -webkit-flex; display: flex;',
		flexWrapWrap = ' -ms-flex-wrap: wrap; flex-wrap: wrap;',
		flexWrapNoWrap = ' -ms-flex-wrap: nowrap; flex-wrap: nowrap;',
		flexDirectionColumn = ' -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column;',
		flexShrink = '-ms-flex-negative: 0; flex-shrink: 0;',
		alignItemsCenter = ' -webkit-box-align: center;-ms-flex-align: center;align-items: center;',
		alignItemsFlexStart = ' -webkit-box-align: start; -ms-flex-align: start; align-items: flex-start;',
		alignItemsFlexEnd = ' -webkit-box-align: end; -ms-flex-align: end; align-items: flex-end;',
		alignItemsBaseline = ' -webkit-box-align: baseline; -ms-flex-align: baseline; align-items: baseline;',
		formData = {},
		demoContentWrap = $('.c-add-block__preview-content'),
		defaultDemoData = {
			wrapper: {},
			imageWrap: {},
			img: {
				src: "https://via.placeholder.com/150",
				alt: "demo-img"
			},
			caption: {},
			title: {
				txt: ""
			},
			link: {
				href: "#",
				txt: ""
			},
			logo: {
				src: "s/images/tmp_file/small-logo.svg",
				// src: "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNDdweCIgaGVpZ2h0PSIxMnB4IiB2aWV3Qm94PSIwIDAgNDcgMTIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogc2tldGNodG9vbCA1NC4xICg3NjQ5MCkgLSBodHRwczovL3NrZXRjaGFwcC5jb20gLS0+CiAgICA8dGl0bGU+ODE3NzQ4MjgtOUJEMi00MTdFLUI1OEUtQUFENTBFQkFGREFDPC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBza2V0Y2h0b29sLjwvZGVzYz4KICAgIDxnIGlkPSJQYWdlLTEiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSJUZWFzZXJfcHJldmlldyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTczMS4wMDAwMDAsIC04MjEuMDAwMDAwKSIgZmlsbC1ydWxlPSJub256ZXJvIj4KICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTQtQ29weS0zIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg0MTguMDAwMDAwLCA0MTguMDAwMDAwKSI+CiAgICAgICAgICAgICAgICA8ZyBpZD0iMSI+CiAgICAgICAgICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTExLUNvcHktOSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzA3LjAwMDAwMCwgMzk5LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAtMTMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDYuMDAwMDAwLCA0LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IlBhdGgiIGZpbGw9IiMwMDI3NTAiIHBvaW50cz0iNDMuMDc2NyAxLjA1ODg1IDQ0LjE2MDkgMS4wNTg4NSA0NC4yMTYyNSAxLjA1ODg1IDQ0LjIxNjI1IDEuMTE0MDUgNDQuMjE2MjUgOC43MDgxIDQ0LjIxNjI1IDguNzU5MjUgNDQuMTYwOSA4Ljc1OTI1IDQzLjA3NjcgOC43NTkyNSA0My4wMjE2NSA4Ljc1OTI1IDQzLjAyMTY1IDguNzA4MSA0My4wMjE2NSAxLjExNDA1IDQzLjAyMTY1IDEuMDU4ODUiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0zOC41NzQsNi41MDk4NSBDMzguNTc0LDYuNzY5MiAzOC41OTA5NSw2Ljk3NzU1IDM4LjYyMDY1LDcuMTQzNDUgQzM4LjYzMzU1LDcuMjI4NSAzOC42NTkwNSw3LjMwNTE1IDM4LjY4NDU1LDcuMzY4NzUgQzM4LjcxNDQsNy40MzI1IDM4Ljc0ODQ1LDcuNDg3ODUgMzguNzkwOSw3LjUzNDUgQzM4LjgzMzM1LDcuNTgxMTUgMzguODg0MzUsNy42MTk1NSAzOC45NDM5LDcuNjUzNiBDMzkuMDAzNDUsNy42ODc2NSAzOS4wNzE3LDcuNzEzIDM5LjE1MjI1LDcuNzMwMSBDMzkuMjMzMSw3Ljc0NzA1IDM5LjMyNjU1LDcuNzU5OTUgMzkuNDMyOSw3Ljc3MjU1IEMzOS41NDM2LDcuNzgxMSAzOS42NjI1NSw3Ljc4NTQ1IDM5Ljc5ODYsNy43ODU0NSBMNDAuNTU1NjUsNy43ODU0NSBDNDAuNzE3MDUsNy43ODU0NSA0MC44OTU4NSw3Ljc4NTQ1IDQxLjA4MzA1LDcuNzgxMSBDNDEuMjc4NSw3Ljc3NzA1IDQxLjQ5OTYsNy43NzI1NSA0MS43NDYzNSw3Ljc2NDE1IEw0MS44MTQzLDcuNzY0MTUgTDQxLjgwMTQsNy44MzIyNSBMNDEuNjE0NSw4LjcyMSBMNDEuNjA1OTUsOC43NjM0NSBMNDEuNTYzNSw4Ljc2MzQ1IEw0MS4yNjU5LDguNzcyIEw0MS4yNjU5LDguNzcyIEw0MS4wMTQ4LDguNzc2MiBMNDEuMDE0OCw4Ljc3NjIgQzQwLjkzODMsOC43NzYyIDQwLjg2Niw4Ljc4MDQgNDAuNzk4MDUsOC43ODA0IEM0MC43MTcyLDguNzgwNCA0MC42NDkyNSw4Ljc4MDQgNDAuNTk4MjUsOC43ODA0IEw0MC4yMTk4LDguNzgwNCBMMzkuNzk0Nyw4Ljc4MDQgQzM5LjU0Nzk1LDguNzgwNCAzOS4zMjI4LDguNzY3NjUgMzkuMTIyODUsOC43Mzc5NSBDMzguOTE4Nyw4LjcwODEgMzguNzQwMiw4LjY2NTY1IDM4LjU4Mjg1LDguNjEwMyBDMzguNDI1NSw4LjU1MDc1IDM4LjI4NTI1LDguNDgyNjUgMzguMTYxOCw4LjQwMTggQzM4LjAzODUsOC4zMTY5IDM3LjkyNzk1LDguMjIzNDUgMzcuODM4Nyw4LjExNzEgQzM3Ljc0OTQ1LDguMDEwNzUgMzcuNjcyOTUsNy44OTE4IDM3LjYwOTA1LDcuNzU5OTUgQzM3LjU0OTY1LDcuNjMyMyAzNy41MDI3LDcuNDg3ODUgMzcuNDY4NjUsNy4zMzQ3IEMzNy40MzkxLDcuMTg1OSAzNy40MTMzLDcuMDI0MzUgMzcuMzk2MzUsNi44NTg0NSBDMzcuMzc5NCw2LjY5MjU1IDM3LjM3MDg1LDYuNTE4MjUgMzcuMzcwODUsNi4zMzU0IEwzNy4zNzA4NSw1Ljc2MTM1IEMzNy4zNzA4NSw1LjU2NTc1IDM3LjM3OTQsNS4zODI5IDM3LjM5NjM1LDUuMjA0NCBDMzcuNDEzMyw1LjAyNTkgMzcuNDM5MSw0Ljg2IDM3LjQ3MzE1LDQuNzAyNjUgQzM3LjUwNzIsNC41NDU0NSAzNy41NTM3LDQuMzk2NSAzNy42MTc2LDQuMjYwNDUgQzM3LjY4MTUsNC4xMjQ0IDM3Ljc1OCw0LjAwMTEgMzcuODQ3MjUsMy44OTA1NSBDMzcuOTM2NjUsMy43OCAzOC4wNDcwNSwzLjY4MjIgMzguMTc0NTUsMy41OTI5NSBDMzguMzAyMzUsMy41MDc5IDM4LjQ0MjYsMy40MzU2IDM4LjYwNCwzLjM3NjA1IEMzOC43NjU3LDMuMzE2NSAzOC45NDg1NSwzLjI3NDA1IDM5LjE1NjksMy4yNDQyIEMzOS4zNjUyNSwzLjIxNDUgMzkuNTk0OSwzLjIwMTc1IDM5Ljg0NTcsMy4yMDE3NSBDNDAuMDc5NTUsMy4yMDE3NSA0MC4yOTIyNSwzLjIxNDUgNDAuNDgzNSwzLjI0IEM0MC42NzQ3NSwzLjI2NTUgNDAuODQ5MiwzLjMwODEgNDAuOTk4LDMuMzYzMyBDNDEuMTQ2OCwzLjQxODUgNDEuMjgzLDMuNDgyMjUgNDEuNDAxOTUsMy41NTQ1NSBDNDEuNTIwOSwzLjYyNjg1IDQxLjYyNzI1LDMuNzExOSA0MS43MTIxNSwzLjgwNTM1IEM0MS44MDE1NSwzLjg5ODk1IDQxLjg3Mzg1LDQuMDAwOTUgNDEuOTMzNCw0LjEwNzMgQzQxLjk5Mjk1LDQuMjEzNjUgNDIuMDM5NzUsNC4zMjg0IDQyLjA2OTYsNC40NTE3IEM0Mi4wOTkxNSw0LjU3MDggNDIuMTI0NjUsNC42OTQxIDQyLjE0MTYsNC44MTc0IEw0Mi4xNDE2LDQuODE3NCBDNDIuMTU4NTUsNC45NDA3IDQyLjE2NzEsNS4wNjgyIDQyLjE2NzEsNS4xOTU4NSBMNDIuMTY3MSw1LjM0OSBDNDIuMTY3MSw1LjQ3MjMgNDIuMTYzMDUsNS41ODI4NSA0Mi4xNTg1NSw1LjY4MDY1IEM0Mi4xNSw1Ljc3ODQ1IDQyLjE0MTYsNS44Njc3IDQyLjEyNDUsNS45NCBDNDIuMTExOSw2LjAxNjUgNDIuMDkwNDUsNi4wODA0IDQyLjA2OTQ1LDYuMTM5OTUgQzQyLjA0Mzk1LDYuMTk5MzUgNDIuMDE0MSw2LjI1MDUgNDEuOTgwMDUsNi4yOTMxIEM0MS45NDYsNi4zMzU1NSA0MS45MDc3NSw2LjM3Mzk1IDQxLjg2MDgsNi40MDM2NSBMNDEuODYwOCw2LjQwMzY1IEM0MS44MTgzNSw2LjQzMzM1IDQxLjc2NzM1LDYuNDU0NjUgNDEuNzE2MzUsNi40Njc0IEw0MS43MTYzNSw2LjQ2NzQgQzQxLjY2NTM1LDYuNDgwMTUgNDEuNjE0MzUsNi40ODg3IDQxLjU1NDY1LDYuNDk3MSBDNDEuNDk1MjUsNi41MDEzIDQxLjQzNTcsNi41MDU2NSA0MS4zNjc3NSw2LjUwNTY1IEwzOC41NzQzLDYuNTA1NjUgTDM4LjU3NDMsNi41MDk4NSBMMzguNTc0LDYuNTA5ODUgWiBNMzkuNzk4Myw0LjE1ODYgQzM5LjY2MjQsNC4xNTg2IDM5LjUzODk1LDQuMTYyOCAzOS40MzI2LDQuMTc1NTUgQzM5LjMyNjI1LDQuMTg0MSAzOS4yMzI4LDQuMjAxMDUgMzkuMTUxOTUsNC4yMjIzNSBMMzkuMTUxOTUsNC4yMjIzNSBDMzkuMDc1NDUsNC4yMzk0NSAzOS4wMDc1LDQuMjY5MTUgMzguOTQ4MSw0LjMwMzIgQzM4Ljg4ODcsNC4zMzcyNSAzOC44Mzc0LDQuMzc5NyAzOC43OTQ5NSw0LjQzMDcgTDM4Ljc5NDk1LDQuNDMwNyBDMzguNzUyNSw0LjQ4MTcgMzguNzE0NCw0LjU0NTYgMzguNjg4Niw0LjYxNzkgQzM4LjY1OTA1LDQuNjkwMDUgMzguNjM3Niw0Ljc3NTI1IDM4LjYyMDY1LDQuODY4NyBDMzguNTkxMSw1LjA1MTU1IDM4LjU3ODIsNS4yODU0IDM4LjU3NCw1LjU3NDYgTDQxLjAzNTgsNS41NzQ2IEw0MS4wMzU4LDUuMjgxMiBDNDEuMDM1OCw1LjE3MDY1IDQxLjAzMTQ1LDUuMDY4NjUgNDEuMDIyOSw0Ljk3NTA1IEM0MS4wMTQzNSw0Ljg4MTQ1IDQxLjAwMTc1LDQuODAwNzUgNDAuOTgwNDUsNC43MjQyNSBDNDAuOTYzNSw0LjY1MTk1IDQwLjkzOCw0LjU4ODIgNDAuOTAzOTUsNC41MzI4NSBMNDAuOTAzOTUsNC41MzI4NSBDNDAuODc0MSw0LjQ3NzY1IDQwLjgzNiw0LjQzMDg1IDQwLjc4OTA1LDQuMzg4NCBMNDAuNzg5MDUsNC4zODg0IEM0MC43NDIxLDQuMzQ1OCA0MC42OTEyNSw0LjMxMTkgNDAuNjI3NjUsNC4yODIwNSBMNDAuNjI3NjUsNC4yODIwNSBDNDAuNTYzNzUsNC4yNTIzNSA0MC40OTU4LDQuMjI2NyA0MC40MTQ5NSw0LjIwOTc1IEM0MC4zMzQxLDQuMTkyOCA0MC4yNDQ3LDQuMTc1NyA0MC4xNDI4NSw0LjE2NzMgQzQwLjA0MSw0LjE1ODkgMzkuOTI2MSw0LjE1NDU1IDM5Ljc5ODMsNC4xNTQ1NSBMMzkuNzk4Myw0LjE1ODYgWiIgaWQ9IlNoYXBlIiBmaWxsPSIjMDAyNzUwIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iUGF0aCIgZmlsbD0iIzAwMjc1MCIgcG9pbnRzPSIzMy4yNjc0NSA0LjkyMzkgMzIuMjQ3IDguNzIxIDMyLjIzNDEgOC43NTkyNSAzMi4xOTE2NSA4Ljc1OTI1IDMxLjA3NzkgOC43NTkyNSAzMS4wMzUgOC43NTkyNSAzMS4wMjI0IDguNzIxIDI5LjQ0NDg1IDMuNDM1NzUgMjkuNDIzNyAzLjM2NzY1IDI5LjQ5NjE1IDMuMzY3NjUgMzAuNTgwMDUgMy4zNjc2NSAzMC42MjI4IDMuMzY3NjUgMzAuNjM1NCAzLjQxMDEgMzEuNjQ3MyA3LjM2ODc1IDMyLjcxOTA1IDMuNDEwMSAzMi43Mjc2IDMuMzY3NjUgMzIuNzcwMDUgMy4zNjc2NSAzMy44MjQ0IDMuMzY3NjUgMzMuODY2ODUgMy4zNjc2NSAzMy44NzU0IDMuNDEwMSAzNC45MDAyIDcuMzg1NyAzNS45NzE2NSAzLjQxMDEgMzUuOTgwMDUgMy4zNjc2NSAzNi4wMjI4IDMuMzY3NjUgMzcuMDM0NyAzLjM2NzY1IDM3LjExMTIgMy4zNjc2NSAzNy4wODk5IDMuNDM1NzUgMzUuNDc4NDUgOC43MjEgMzUuNDY1NTUgOC43NTkyNSAzNS40MjMxIDguNzU5MjUgMzQuMzA5MiA4Ljc1OTI1IDM0LjI2Njc1IDguNzU5MjUgMzQuMjU4MzUgOC43MjEiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0yNy41MTAzLDUuNTM2MiBDMjcuNjY3NjUsNS41NDg5NSAyNy44MTIxLDUuNTc0NDUgMjcuOTM5NzUsNS42MDQzIEwyNy45Mzk3NSw1LjYwNDMgQzI4LjA2NzI1LDUuNjM4MzUgMjguMTgyLDUuNjgwOCAyOC4yODQsNS43MzE5NSBDMjguMzgxOCw1Ljc4Mjk1IDI4LjQ3MTIsNS44MzgxNSAyOC41NDc3LDUuOTAyMDUgQzI4LjYyNDIsNS45NjU4IDI4LjY5MjQ1LDYuMDMzNzUgMjguNzQ3NSw2LjEwNjA1IEMyOC44MDI4NSw2LjE3ODM1IDI4Ljg0NTMsNi4yNTQ4NSAyOC44ODM3LDYuMzMxMzUgTDI4Ljg4MzcsNi4zMzEzNSBDMjguOTIxOCw2LjQwNzg1IDI4Ljk0NzMsNi40ODg3IDI4Ljk2ODYsNi41NzM3NSBDMjguOTg5NzUsNi42NTQ2IDI5LjAwMjY1LDYuNzM1MyAyOS4wMTEwNSw2LjgxNjE1IEMyOS4wMTk0NSw2Ljg5NyAyOS4wMjM5NSw2Ljk3MzUgMjkuMDIzOTUsNy4wNSBMMjkuMDIzOTUsNy4xOTQ2IEMyOS4wMjM5NSw3LjY5NjM1IDI4Ljg0MDgsOC4wODc0IDI4LjQ3NTI1LDguMzY4MDUgQzI4LjI5MjQsOC41MDg0NSAyOC4wNjMwNSw4LjYxMDQ1IDI3Ljc4NjQ1LDguNjc4NTUgQzI3LjUxMDE1LDguNzQ2NSAyNy4xOTExLDguNzgwNTUgMjYuODIxMzUsOC43ODA1NSBDMjYuNjk4MDUsOC43ODA1NSAyNi41Mjc4LDguNzgwNTUgMjYuMzE5NDUsOC43ODA1NSBMMjUuNzQxMiw4Ljc3NjM1IEMyNS41MzczNSw4Ljc3NjM1IDI1LjMyODcsOC43NzIxNSAyNS4xMjQ4NSw4Ljc2NzggQzI0LjkyMDcsOC43NjM2IDI0LjcxMjM1LDguNzYzNiAyNC41MDgyLDguNzU5MjUgTDI0LjQ0MDI1LDguNzU5MjUgTDI0LjQ1MzE1LDguNjk1NSBMMjQuNjQwMDUsNy44MDY3NSBMMjQuNjQ4Niw3Ljc2NDE1IEwyNC42OTU1NSw3Ljc2NDE1IEMyNC44ODI3NSw3Ljc2ODUgMjUuMDY5NjUsNy43NzI3IDI1LjI1Njg1LDcuNzcyNyBDMjUuNDQ4MSw3Ljc3NzA1IDI1LjYzOTUsNy43NzcwNSAyNS44MzA3NSw3Ljc3NzA1IEMyNi4wMjIsNy43NzcwNSAyNi4yMDQ4NSw3Ljc4MTEgMjYuMzc1MSw3Ljc4MTEgQzI2LjUxOTU1LDcuNzgxMSAyNi42ODk4LDcuNzgxMSAyNi44NzI1LDcuNzgxMSBDMjcuMDY3OTUsNy43ODExIDI3LjIzMzcsNy43Njg1IDI3LjM2OTksNy43NDcwNSBDMjcuNDk3NCw3LjcyNTkgMjcuNTk5NTUsNy42OTYwNSAyNy42Njc1LDcuNjUzNiBDMjcuNzMxMSw3LjYxNTIgMjcuNzc3OSw3LjU2IDI3LjgxMTk1LDcuNDgzNSBDMjcuODQ2LDcuNDAyNjUgMjcuODYyOTUsNy4zMDA1IDI3Ljg2Mjk1LDcuMTgxNTUgQzI3Ljg2Mjk1LDcuMTA1MDUgMjcuODU0NCw3LjAyODQgMjcuODM3NDUsNi45NjQ2NSBMMjcuODM3NDUsNi45NjQ2NSBDMjcuODI4OSw2LjkzNDk1IDI3LjgyMDUsNi45MDUxIDI3LjgwMzQsNi44NzU0IEMyNy43OTA4LDYuODQ5OSAyNy43NjkzNSw2LjgyNDQgMjcuNzQ4MzUsNi43OTg5IEwyNy43NDgzNSw2Ljc5ODkgQzI3LjcyNjksNi43NzM0IDI3LjY5NzA1LDYuNzUyMSAyNy42NjMxNSw2LjczMDggQzI3LjYyOTI1LDYuNzA5NSAyNy41OTExNSw2LjY5MjU1IDI3LjU0NDIsNi42NzEyNSBMMjcuNTQ0Miw2LjY3MTI1IEMyNy40OTcyNSw2LjY1NDMgMjcuNDQ2NCw2LjYzNzIgMjcuMzgyNSw2LjYyODggQzI3LjMxODksNi42MTYwNSAyNy4yNDY2LDYuNjA3NSAyNy4xNjU3NSw2LjU5ODk1IEwyNy4xNjU3NSw2LjU5ODk1IEwyNS45Mjg1NSw2LjQ4ODQgTDI1LjkyODU1LDYuNDg4NCBDMjUuNzk2Nyw2LjQ3OTg1IDI1LjY3Nzc1LDYuNDU4NyAyNS41NjcwNSw2LjQzMzA1IEMyNS40NTY2NSw2LjQwNzU1IDI1LjM1MDMsNi4zNjkzIDI1LjI2MDksNi4zMjY4NSBDMjUuMTY3NDUsNi4yODQyNSAyNS4wODI1NSw2LjIzMzI1IDI1LjAxMDEsNi4xNzgwNSBDMjQuOTMzNiw2LjEyMjcgMjQuODY5Nyw2LjA2MzMgMjQuODEwMyw1Ljk5NTIgQzI0Ljc1MDksNS45MzE0NSAyNC43MDM5NSw1Ljg1OTE1IDI0LjY2MTUsNS43ODY4NSBDMjQuNjE5MDUsNS43MTQ1NSAyNC41ODUsNS42MzM3IDI0LjU1OTIsNS41NTMgQzI0LjUzMzcsNS40NzIxNSAyNC41MTI1NSw1LjM5MTQ1IDI0LjQ5OTgsNS4zMDY0IEMyNC40ODcyLDUuMjIxMzUgMjQuNDc4NjUsNS4xNDA2NSAyNC40Nzg2NSw1LjA1NTYgTDI0LjQ3ODY1LDQuODgxMyBDMjQuNDc4NjUsNC42NDMyNSAyNC41MjExLDQuNDI2MzUgMjQuNjA2MTUsNC4yMzkzIEMyNC42OTEwNSw0LjA1MjEgMjQuODE4ODUsMy44ODYzNSAyNC45ODQ2LDMuNzUwMyBDMjUuMTUwMzUsMy42MTQyNSAyNS4zNzE2LDMuNTEyMjUgMjUuNjQ3OSwzLjQ0NDE1IEMyNS45MTU2NSwzLjM3NjA1IDI2LjIzODc1LDMuMzQyMTUgMjYuNjEzLDMuMzQyMTUgTDI3LjY0NjM1LDMuMzQyMTUgQzI3Ljk5OTQ1LDMuMzQyMTUgMjguNDAzNCwzLjM1MDcgMjguODU4MzUsMy4zNjM0NSBMMjguOTIxOTUsMy4zNjM0NSBMMjguOTA5MzUsMy40MjcyIEwyOC43MjIxNSw0LjMxNTk1IEwyOC43MTM2LDQuMzU4NCBMMjguNjY2NjUsNC4zNTg0IEwyOC4yNTg1LDQuMzQ5ODUgTDI3LjkwMTM1LDQuMzQxMyBMMjcuNTkxMTUsNC4zMzcxIEMyNy41MTAzLDQuMzM3MSAyNy40MjA5LDQuMzM3MSAyNy4zMTkwNSw0LjMzNzEgTDI2Ljg2NDEsNC4zMzcxIEwyNi40OTg0LDQuMzM3MSBDMjYuMzI0MSw0LjMzNzEgMjYuMTc5MzUsNC4zNDU2NSAyNi4wNjA0LDQuMzY2OCBDMjUuOTQ1NSw0LjM4ODEgMjUuODU2MjUsNC40MTc4IDI1Ljc5MjM1LDQuNDU2MiBMMjUuNzkyMzUsNC40NTYyIEMyNS43MzI5NSw0LjQ5NDQ1IDI1LjY4NjMsNC41NDU0NSAyNS42NTY0NSw0LjYwOTIgQzI1LjYyNjYsNC42NzczIDI1LjYwOTUsNC43NjIzNSAyNS42MDk1LDQuODY0MzUgQzI1LjYwOTUsNC45MDY4IDI1LjYxMzg1LDQuOTQ1MiAyNS42MTM4NSw0Ljk4MzMgQzI1LjYxNzksNS4wMjE1NSAyNS42MjI0LDUuMDU1NiAyNS42MzA4LDUuMDg1MyBDMjUuNjM5MzUsNS4xMTUxNSAyNS42NDc3NSw1LjE0MDY1IDI1LjY2NDg1LDUuMTY2MTUgTDI1LjY2NDg1LDUuMTY2MTUgQzI1LjY3Nzc1LDUuMTkxNjUgMjUuNjk4OSw1LjIxMjk1IDI1LjcyMDIsNS4yMzQyNSBDMjUuNzQxMzUsNS4yNTU1NSAyNS43NzEyLDUuMjcyNSAyNS44MDUxLDUuMjkzOCBDMjUuODM5MTUsNS4zMTA3NSAyNS44ODE2LDUuMzMyMDUgMjUuOTI4NCw1LjM0NDggQzI1Ljk3NTA1LDUuMzYxNzUgMjYuMDMwNCw1LjM3NDUgMjYuMDk0MTUsNS4zODcyNSBDMjYuMTU3OSw1LjQgMjYuMjMwMDUsNS40MDg1NSAyNi4zMTA5LDUuNDEyNzUgTDI2LjMxMDksNS40MTI3NSBMMjcuNTEsNS41MjMzIEwyNy41MSw1LjUzNjIgTDI3LjUxMDMsNS41MzYyIFoiIGlkPSJQYXRoIiBmaWxsPSIjMDAyNzUwIj48L3BhdGg+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMjIuMzk5NSw3Ljc5NCBMMjIuMzk5NSw1LjQ2MzkgQzIyLjM5OTUsNS4zMzIwNSAyMi4zOTUxNSw1LjIxMzEgMjIuMzgyNTUsNS4xMDY3NSBMMjIuMzgyNTUsNS4xMDY3NSBDMjIuMzc0LDUuMDAwNCAyMi4zNTcwNSw0LjkwNjggMjIuMzM1Niw0LjgyMTkgQzIyLjMxNDE1LDQuNzQxMDUgMjIuMjg4NjUsNC42Njg3NSAyMi4yNTQ3NSw0LjYwOTM1IEMyMi4yMjA3LDQuNTQ5OTUgMjIuMTgyNzUsNC40OTQ2IDIyLjEzNTgsNC40NTIgQzIyLjA4ODg1LDQuNDA5NTUgMjIuMDMzOCw0LjM3MTE1IDIxLjk2NTU1LDQuMzQxNDUgQzIxLjkwMTk1LDQuMzExNzUgMjEuODI1MTUsNC4yOTA0NSAyMS43NDAyNSw0LjI3MzM1IEMyMS42NTA4NSw0LjI1NjQgMjEuNTUzMzUsNC4yNDc4NSAyMS40NDI2NSw0LjIzOTMgQzIxLjMyNzc1LDQuMjMwNzUgMjEuMjA0NDUsNC4yMzA3NSAyMS4wNjg1NSw0LjIzMDc1IEMyMC45MjgxNSw0LjIzNDk1IDIwLjc5NjQ1LDQuMjM5MyAyMC42ODE1NSw0LjI1MjA1IEMyMC41NjY2NSw0LjI2NDggMjAuNDY5MTUsNC4yODE3NSAyMC4zODM5NSw0LjMwNzQgQzIwLjMwMzQsNC4zMjg3IDIwLjIyNjYsNC4zNTg0IDIwLjE2Myw0LjQwMSBDMjAuMDk5MSw0LjQzOTI1IDIwLjA0ODEsNC40ODYwNSAyMC4wMDEzLDQuNTQxMjUgQzE5Ljk1NDM1LDQuNTk2NiAxOS45MjA0NSw0LjY2NDU1IDE5Ljg5MDksNC43NDEyIEMxOS44NjEwNSw0LjgyMjA1IDE5LjgzNTU1LDQuOTExMyAxOS44MjI5NSw1LjAxMzMgQzE5LjgwNiw1LjExOTY1IDE5Ljc5NzE1LDUuMjM4NiAxOS43ODg2LDUuMzcwNDUgQzE5Ljc4MDA1LDUuNTAyMyAxOS43NzYsNS42NTUzIDE5Ljc3Niw1LjgyMTIgTDE5Ljc3Niw2LjIwODIgQzE5Ljc3Niw2LjM4NjcgMTkuNzgwMDUsNi41NDQwNSAxOS43ODg2LDYuNjg0NDUgQzE5Ljc5NzE1LDYuODI0NyAxOS44MTAwNSw2Ljk1MjM1IDE5LjgyMjk1LDcuMDU4NTUgQzE5LjgzNTU1LDcuMTY0OSAxOS44NjEwNSw3LjI1ODM1IDE5Ljg5MDksNy4zMzkyIEMxOS45MjA0NSw3LjQyMDA1IDE5Ljk1ODg1LDcuNDg4IDIwLjAwMTMsNy41NDMyIEMyMC4wNDM3NSw3LjU5ODQgMjAuMDk5MSw3LjY0NTM1IDIwLjE2Myw3LjY3OTI1IEwyMC4xNjMsNy42NzkyNSBDMjAuMjI2Niw3LjcxNzUgMjAuMjk4OSw3Ljc0MzE1IDIwLjM4Mzk1LDcuNzYwMSBDMjAuNDY5MTUsNy43NzcwNSAyMC41NzExNSw3Ljc4OTggMjAuNjkwMSw3Ljc5ODM1IEMyMC44MDkzNSw3LjgwNjkgMjAuOTQxMiw3LjgxMTEgMjEuMDg1NjUsNy44MDY5IEwyMi4zOTUxNSw3Ljc4NTYgTDIyLjM5OTUsNy43OTQgWiBNMjEuMDMwMTUsOC44MDU5IEMyMC44MDA4LDguODEwMSAyMC41ODgxLDguODAxNyAyMC4zOTY3LDguNzgwNCBDMjAuMjAwOTUsOC43NTkxIDIwLjAyNjY1LDguNzI5NCAxOS44NjkzLDguNjgyNiBDMTkuNzExOTUsOC42MzU4IDE5LjU2NzUsOC41NzYyNSAxOS40Mzk4NSw4LjUwNDEgQzE5LjMxMjM1LDguNDI3NiAxOS4xOTc2LDguMzM4MzUgMTkuMTA0MTUsOC4yMzYyIEMxOS4wMDYzNSw4LjEzIDE4LjkyNTUsOC4wMTA5IDE4Ljg1NzQsNy44NzQ4NSBDMTguNzg5Myw3LjczODggMTguNzM0MSw3LjU5IDE4LjY5Niw3LjQyNDEgQzE4LjY1NzYsNy4yNTgzNSAxOC42MjM3LDcuMDc5NyAxOC42MDY2LDYuODc5OSBDMTguNTg1MTUsNi42ODAxIDE4LjU3Njc1LDYuNDYzMiAxOC41NzY3NSw2LjIzMzU1IEwxOC41NzY3NSw1Ljg0NjU1IEMxOC41NzY3NSw1LjYzODIgMTguNTg1Myw1LjQ0MjYgMTguNjAyMjUsNS4yNTk3NSBDMTguNjE5Miw1LjA3NjkgMTguNjQ0Nyw0LjkwMjYgMTguNjc4NzUsNC43NDUyNSBDMTguNzEzMSw0LjU4NzkgMTguNzYzOTUsNC40MzkxIDE4LjgyNzU1LDQuMzAzMDUgQzE4Ljg5MTQ1LDQuMTY3IDE4Ljk2Nzk1LDQuMDQzNyAxOS4wNjE3LDMuOTMzMTUgQzE5LjE1NTE1LDMuODIyNiAxOS4yNjU1NSwzLjcyNDggMTkuMzg5LDMuNjM5NzUgQzE5LjUxMjQ1LDMuNTU0NyAxOS42NTcwNSwzLjQ4NjYgMTkuODE0NCwzLjQyNzIgQzE5Ljk3MTc1LDMuMzcyIDIwLjE0NjA1LDMuMzI5NCAyMC4zNDU4NSwzLjI5NTUgQzIwLjU0MTMsMy4yNjU2NSAyMC43NTgzNSwzLjI0ODcgMjAuOTk2MjUsMy4yNDQ1IEMyMS4xNjIsMy4yNDAzIDIxLjMyMzg1LDMuMjQ4NyAyMS40NzI2NSwzLjI2MTQ1IEMyMS42MjE0NSwzLjI3ODU1IDIxLjc2MTg1LDMuMzA0MDUgMjEuODk3NzUsMy4zMzc5NSBDMjIuMDI5NiwzLjM3MiAyMi4xNDQ1LDMuNDE0NDUgMjIuMjQ2MzUsMy40NjEyNSBDMjIuMzA1NzUsMy40OTA5NSAyMi4zNjEyNSwzLjUyMDggMjIuNDEyMSwzLjU1NDg1IEwyMi40MTIxLDEuMTMxMyBMMjIuNDEyMSwxLjA3NTk1IEwyMi40Njc0NSwxLjA3NTk1IEwyMy41NTE4LDEuMDU5IEwyMy42MDY4NSwxLjA1OSBMMjMuNjA2ODUsMS4xMTQyIEwyMy42MDY4NSw3LjgyODA1IEMyMy42MDY4NSw4LjE1NTUgMjMuNTMwMzUsOC4zOTc5IDIzLjM3Myw4LjU0NjcgQzIzLjI5MjQ1LDguNjIzMiAyMy4xODE3NSw4LjY3ODU1IDIzLjAzMjgsOC43MjEgQzIyLjg4ODM1LDguNzU5MjUgMjIuNzE0MDUsOC43ODA0IDIyLjUwMTM1LDguNzgwNCBMMjEuMDQyOSw4LjgwNTkgTDIxLjAzMDE1LDguODA1OSBaIiBpZD0iU2hhcGUiIGZpbGw9IiMwMDI3NTAiPjwvcGF0aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNC4xNDIxNSw3LjE3MzE1IEMxNC4xNDIxNSw3LjI4MzU1IDE0LjE1OTEsNy4zODE1IDE0LjE5MzE1LDcuNDY2NCBDMTQuMjI3Miw3LjU0NzI1IDE0LjI3NCw3LjYxNTIgMTQuMzM3Niw3LjY2NjM1IEMxNC40MDE1LDcuNzE3MzUgMTQuNDk5Myw3Ljc1OTk1IDE0LjYyMjYsNy43ODU0NSBDMTQuNzUwMSw3LjgxMDk1IDE0LjkxMTgsNy44MjM3IDE1LjA5ODcsNy44MjM3IEwxNi42MTI1LDcuODAyNCBMMTYuNjEyNSw2LjkxMzY1IEMxNi42MTI1LDYuODQ5OSAxNi42MDgxNSw2Ljc5MDM1IDE2LjYwMzk1LDYuNzM5MzUgQzE2LjU5NTQsNi42ODQxNSAxNi41ODcsNi42MzczNSAxNi41NzQxLDYuNTkwNTUgQzE2LjU2MTUsNi41NDgxIDE2LjU0NDU1LDYuNTE0MDUgMTYuNTE5MDUsNi40OCBMMTYuNTE5MDUsNi40OCBDMTYuNDkzNTUsNi40NDU5NSAxNi40NjM3LDYuNDIwNDUgMTYuNDI5NjUsNi4zOTQ5NSBMMTYuNDI5NjUsNi4zOTQ5NSBDMTYuMzkxMjUsNi4zNjk0NSAxNi4zNDQ3NSw2LjM0ODE1IDE2LjI4OTI1LDYuMzI2ODUgTDE2LjI4OTI1LDYuMzI2ODUgQzE2LjIyOTg1LDYuMzA1NTUgMTYuMTY1OTUsNi4yOTI4IDE2LjA4OTQ1LDYuMjgwMDUgQzE2LjAxMjk1LDYuMjY3MyAxNS45MjM3LDYuMjU4NzUgMTUuODE3MzUsNi4yNTQ1NSBDMTUuNzExLDYuMjUwMzUgMTUuNTk2MSw2LjI0NiAxNS40Njg3NSw2LjI1MDM1IEwxNS4yMzA1NSw2LjI1NDU1IEMxNS4xMjAxNSw2LjI1NDU1IDE1LjAxNzg1LDYuMjU4NzUgMTQuOTI0NCw2LjI2NzMgQzE0LjgzMDk1LDYuMjc1ODUgMTQuNzUwMSw2LjI4NDI1IDE0LjY3NzY1LDYuMjk3MTUgQzE0LjYwNTY1LDYuMzA5OSAxNC41NDE3NSw2LjMyNjg1IDE0LjQ4NjQsNi4zNDgxNSBMMTQuNDg2NCw2LjM0ODE1IEMxNC40MzEzNSw2LjM2OTQ1IDE0LjM4NDQsNi4zOTA3NSAxNC4zNDYsNi40MTYyNSBDMTQuMjY5NSw2LjQ2NzI1IDE0LjIxODUsNi41MzUzNSAxNC4xODQ2LDYuNjIwNCBDMTQuMTY3NjUsNi42NjcyIDE0LjE1NDc1LDYuNzE0IDE0LjE1MDU1LDYuNzczNTUgTDE0LjE1MDU1LDYuNzczNTUgQzE0LjE0Miw2LjgyODc1IDE0LjEzNzY1LDYuODkyNSAxNC4xMzc2NSw2Ljk2NDggTDE0LjEzNzY1LDcuMTc3MzUgTDE0LjE0MjE1LDcuMTczMTUgWiBNMTcuNzU2MjUsNy44MjgwNSBDMTcuNzU2MjUsNy45OTgxNSAxNy43MzUxLDguMTQyNzUgMTcuNjk2ODUsOC4yNjE3IEMxNy42NTQ0LDguMzg1IDE3LjU5NDg1LDguNDgyOTUgMTcuNTA5NjUsOC41NTUyNSBDMTcuNDI5MSw4LjYyNzQgMTcuMzE0Miw4LjY4Mjc1IDE3LjE2NTQsOC43MTY2NSBDMTcuMDI1LDguNzUwODUgMTYuODUwNyw4Ljc3MiAxNi42NDY1NSw4Ljc3MiBMMTUuMDk4ODUsOC43OTMzIEMxNC43MzMxNSw4Ljc5NzUgMTQuNDE4Niw4Ljc2NzggMTQuMTU0OSw4LjcwODI1IEMxMy44ODcxNSw4LjY0NDUgMTMuNjcwNCw4LjU0Njg1IDEzLjUwODU1LDguNDEwNjUgQzEzLjM0MjgsOC4yNzQ3NSAxMy4yMjM4NSw4LjEwNDUgMTMuMTM4OCw3LjkwMDY1IEMxMy4wNTc5NSw3LjY5NjUgMTMuMDE1NSw3LjQ2MjY1IDEzLjAxNTUsNy4xOTQ3NSBMMTMuMDE1NSw2Ljk2OTQ1IEMxMy4wMTU1LDYuNjc2MDUgMTMuMDU3OTUsNi40MjUyNSAxMy4xNDczNSw2LjIxMjcgQzEzLjIzNjQ1LDUuOTk1OCAxMy4zNjgzLDUuODI1NyAxMy41NDY5NSw1LjY5NCBDMTMuNzIxMjUsNS41NjY1IDEzLjk0NjU1LDUuNDY0MzUgMTQuMjIyODUsNS40MDA2IEMxNC40OTA5LDUuMzMyNSAxNC44MDk2NSw1LjI5ODYgMTUuMTc1MzUsNS4yOTQ0IEMxNS4zODQsNS4yOTAyIDE1LjU3MDksNS4yOTg2IDE1LjczMjYsNS4zMTEzNSBDMTUuODk4MzUsNS4zMjQxIDE2LjAzODc1LDUuMzQ5NiAxNi4xNjIwNSw1LjM3OTQ1IEMxNi4yODEsNS40MDkzIDE2LjM4Myw1LjQ0MzIgMTYuNDcyMjUsNS40NzcyNSBDMTYuNTIzMjUsNS40OTg1NSAxNi41NzAwNSw1LjUxNTUgMTYuNjA4MTUsNS41MzY4IEwxNi42MDgxNSw1LjMyNDI1IEMxNi42MDgxNSw1LjIzOTIgMTYuNjA0MSw1LjE2MjcgMTYuNTk5Niw1LjA5MDQgQzE2LjU5NTU1LDUuMDE4MSAxNi41ODcsNC45NSAxNi41NzQxLDQuODkwNDUgQzE2LjU2MTUsNC44MzUxIDE2LjU0NDU1LDQuNzc5OSAxNi41MTkwNSw0LjczMzEgQzE2LjQ5MzU1LDQuNjg2MyAxNi40NTkzNSw0LjY0Mzg1IDE2LjQxNjc1LDQuNjA1NDUgTDE2LjQxNjc1LDQuNjA1NDUgQzE2LjM3NDMsNC41NjcyIDE2LjMyMzMsNC41MzMxNSAxNi4yNTk0LDQuNTAzNDUgQzE2LjE5NTgsNC40NzM3NSAxNi4xMjM1LDQuNDQ4MSAxNi4wMzg0NSw0LjQyNjk1IEMxNS45NTMyNSw0LjQwNTY1IDE1Ljg1MTI1LDQuMzkyOSAxNS43MzYzNSw0LjM4NDM1IEMxNS42MTc0LDQuMzc1OCAxNS40ODEyLDQuMzcxNiAxNS4zMzY3NSw0LjM3MTYgQzE1LjE5MjMsNC4zNzE2IDE1LjA0MzUsNC4zNzU4IDE0Ljg5MDIsNC4zODAxNSBDMTQuNzM3Miw0LjM4NDM1IDE0LjU4LDQuMzg4NyAxNC40MTQxLDQuMzg4NyBDMTQuMjUyNCw0LjM5MjkgMTQuMDk5NCw0LjM5NzI1IDEzLjk1OTE1LDQuNDAxNDUgQzEzLjgxODksNC40MDU2NSAxMy42OTExLDQuNDEgMTMuNTcyMTUsNC40MTQyIEwxMy40OTk4NSw0LjQxODQgTDEzLjUxNjgsNC4zNTAzIEwxMy43MTI1NSwzLjQ1NzM1IEwxMy43MjExLDMuNDE0NzUgTDEzLjc2MzU1LDMuNDE0NzUgQzEzLjg1NywzLjQxMDU1IDEzLjk2NzcsMy40MDYyIDE0LjA4NjY1LDMuNDAyIEwxNC4wODY2NSwzLjQwMiBDMTQuMjA1NiwzLjM5NzggMTQuMzMzNCwzLjM5MzQ1IDE0LjQ3NzcsMy4zODkyNSBDMTQuNjE4MSwzLjM4NTA1IDE0Ljc2MjcsMy4zODA3IDE0LjkwMzEsMy4zODA3IEwxNC45MDMxLDMuMzgwNyBDMTUuMDQzNSwzLjM3NjUgMTUuMTg3OCwzLjM3NjUgMTUuMzMyMjUsMy4zNzIxNSBDMTUuNzUzMywzLjM2Nzk1IDE2LjExNDgsMy4zOTc2NSAxNi40MjA5NSwzLjQ2NTc1IEMxNi43MzExNSwzLjUzMzg1IDE2Ljk4MjI1LDMuNjQ0MjUgMTcuMTczNjUsMy43OTMyIEMxNy4zNjUwNSwzLjk0MjE1IDE3LjUxMzg1LDQuMTI0ODUgMTcuNjA3Myw0LjM0NTk1IEMxNy43MDUxLDQuNTY3MDUgMTcuNzUyMDUsNC44MjIyIDE3Ljc1MjA1LDUuMTExNCBMMTcuNzUyMDUsNy44MTk5NSBMMTcuNzU2MjUsNy44MjgwNSBaIiBpZD0iU2hhcGUiIGZpbGw9IiMwMDI3NTAiPjwvcGF0aD4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwb2x5Z29uIGlkPSJQYXRoIiBmaWxsPSIjMDAyNzUwIiBwb2ludHM9IjQ1LjI1ODE1IDEuMDU4ODUgNDYuMzQyMzUgMS4wNTg4NSA0Ni4zOTc0IDEuMDU4ODUgNDYuMzk3NCAxLjExNDA1IDQ2LjM5NzQgOC43MDgxIDQ2LjM5NzQgOC43NTkyNSA0Ni4zNDIzNSA4Ljc1OTI1IDQ1LjI1ODE1IDguNzU5MjUgNDUuMjAyOCA4Ljc1OTI1IDQ1LjIwMjggOC43MDgxIDQ1LjIwMjggMS4xMTQwNSA0NS4yMDI4IDEuMDU4ODUiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwb2x5Z29uIGlkPSJQYXRoIiBmaWxsPSIjMDAyNzUwIiBwb2ludHM9IjEwLjYwODc1IDcuNTY0MzUgMTIuOTYgMTEuODg0MzUgNi40ODAxNSA5LjAwMTY1IDAgMTEuODg0MzUgMi4zNTU3NSA3LjU2NDM1IDQuNTc1MyA2LjQwNzg1IDMuMTkzMzUgOC45NDE5NSA2LjQ4MDE1IDcuNDc5MyA5Ljc3MTE1IDguOTQxOTUgOC4zODUgNi40MDc4NSI+PC9wb2x5Z29uPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IlBhdGgiIGZpbGw9IiMwNUFCRTkiIHBvaW50cz0iOS43NTQwNSA2LjAwMzkgNy41MzQ1IDQuODQzMDUgNi40ODAxNSAyLjkxMjcgNS40MjU1IDQuODQzMDUgMy4yMDU5NSA2LjAwMzkgNi40ODAxNSAtOC41MjY1MTI4M2UtMTQiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgICAgICAgICAgPC9nPgogICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgPC9nPgogICAgICAgIDwvZz4KICAgIDwvZz4KPC9zdmc+",
				alt: "logo-img"
			}
		},
		debounceTime = 900;


	$('.form-group-wrap').find('.radio-group-input').on('change', function() {
		if ($('#selectthematics').prop('checked') == true) {
			$('.c-add-block__tematic-block').slideDown(300);
		} else {
			$('.c-add-block__tematic-block').slideUp(300);
		}
	});



	$('input[name="border-thickness"], [name="tizer-vert"], [name="tizer-goriz"], [name="text-size"], [name="link-size"], [name="block-width"], [name="block-height"],[name="tizer-width"], [name="tizer-height"] ').keyup(debounce(function(){
		var th = $(this),
			v = parseInt(th.val()),
			min = parseInt(th.attr('min')),
			max = parseInt(th.attr('max'));

		if (v < min || th.val().length == 0){
			th.val(min);
		} else if (v > max){
			th.val(max);
		}
		parseConstructorForm();
	}, debounceTime));


	form.find('input[name="proportions"]').on('change', function(){
		onChangeProportions($(this));
	});


	form.find('input[name="tizer-height"]').on('input change', debounce(function(){
		onChangeTizerHeight($(this));

	}, debounceTime));

	form.find('input[name="tizer-width"]').on('input change', debounce(function(){
		onChangeTizerWidth($(this));
	}, debounceTime));

	form.find('input:not([name="proportions"]):not([name="tizer-height"]):not([name="tizer-width"])').on('change input selected', parseConstructorForm);
	parseConstructorForm();
	onChangeProportions(form.find('input[name="proportions"]'));
	// onChangeTizerHeight(form.find('input[name="tizer-height"]'));
	// onChangeTizerWidth(form.find('input[name="tizer-width"]'));
	
	function onChangeProportions(th) {
		console.log('onChangeProportions!!!!!!!');
		var tizerHeightInput = form.find('input[name="tizer-height"]'),
			tizerWidthInput = form.find('input[name="tizer-width"]'),
			tizerHeight = tizerHeightInput.val(),
			tizerMaxWidth = parseInt(tizerWidthInput.attr('max')),
			tizerMinWidth = parseInt(tizerWidthInput.attr('min')),
			tizerMinHeight = parseInt(tizerHeightInput.attr('min')),
			value;
		console.log('Proportions =', th.val());
		if (th.val() === '4x3') {
			value = parseInt(tizerHeight * 4 / 3);
			if (value > tizerMaxWidth) {
				console.log('value > tizerMaxWidth');
				value = tizerMaxWidth;
				tizerHeightInput.val(parseInt(value * 3 / 4));
			} else if (value < tizerMinWidth) {
				console.log('value < tizerMinWidth');
				value = parseInt(tizerMinHeight * 4 / 3);
				tizerHeightInput.val(tizerMinHeight);
			}
			tizerWidthInput.val(value);
		} else {
			// 1x1
			value = tizerHeight;
			if (value > tizerMaxWidth) {
				value = tizerMaxWidth;
			} else if (value < tizerMinWidth) {
				value = tizerMinWidth;
			}
			tizerWidthInput.val(value);
		}
		parseConstructorForm();
	}
	
	function onChangeTizerWidth(th) {
		console.log('onChangeTizerWidth!!!!!!!');
		var tizerProportions = form.find('input[name="proportions"]:checked'),
			tizerHeightInput = form.find('input[name="tizer-height"]'),
			tizerWidth = th.val(),
			tizerProportionsValue = tizerProportions.val(),
			tizerMaxHeight = parseInt(tizerHeightInput.attr('max')),
			tizerMinHeight = parseInt(tizerHeightInput.attr('min')),
			tizerMaxWidth = parseInt(th.attr('max')),
			tizerMinWidth = parseInt(th.attr('min')),
			value;
		console.log('Proportions =', tizerProportionsValue);
		if (tizerProportionsValue === '4x3') {
			value = parseInt(tizerWidth * 3 / 4);
			if (value > tizerMaxHeight) {
				console.log('value > tizerMaxHeight');
				value = tizerMaxWidth;
				th.val(tizerMaxWidth);
			} else if (value < tizerMinWidth) {
				console.log('value < tizerMinWidth');
				value = tizerMinHeight;
				th.val(parseInt(tizerMinHeight * 4 / 3));
			}
		} else {
			// 1x1
			value = tizerWidth;
			if (value > tizerMaxHeight) {
				value = tizerMaxHeight;
			} else if (value < tizerMinHeight) {
				value = tizerMinHeight;
			}
		}
		tizerHeightInput.val(value);
		parseConstructorForm();
	}
	
	function onChangeTizerHeight(th) {
		console.log('onChangeTizerHeight!!!!!!!');
		var tizerProportions = form.find('input[name="proportions"]:checked'),
			tizerWidthInput = form.find('input[name="tizer-width"]'),
			tizerHeight = th.val(),
			tizerProportionsValue = tizerProportions.val(),
			tizerMinHeight = parseInt(th.attr('min')),
			tizerMaxWidth = parseInt(tizerWidthInput.attr('max')),
			tizerMinWidth = parseInt(tizerWidthInput.attr('min')),
			value;
		console.log('Proportions =', tizerProportionsValue);
		if (tizerProportionsValue === '4x3') {
			value = parseInt(tizerHeight * 4 / 3);
			if (value > tizerMaxWidth) {
				console.log('value > tizerMaxWidth');
				value = tizerMaxWidth;
				th.val(parseInt(value * 3 / 4));
			} else if (value < tizerMinWidth) {
				console.log('value < tizerMinWidth');
				value = parseInt(tizerMinHeight * 4 / 3);
				th.val(tizerMinHeight);
			}
		} else {
			// 1x1
			value = tizerHeight;
			if (value > tizerMaxWidth) {
				value = tizerMaxWidth;
			} else if (value < tizerMinWidth) {
				value = tizerMinWidth;
			}
		}
		tizerWidthInput.val(value);
		parseConstructorForm();
	}

	function parseConstructorForm() {

			// $('.after-px-block').text($("input[name='block-value']").val());
		formData = {};
		formData.blockTitle = form.find('[name="block-title"]').val();
		formData.blockPlace = form.find('[name="block-place"]').val();
		formData.blockType = form.find('[name="block-type"]').val();
		formData.thematicSwitch= form.find('[name="thematic-switch"]').val(); ///if else
		formData.themBlock= form.find('[name="them-block"]').val();
		formData.blockValue= form.find('[name="block-value"]').val();
		formData.blockHeight= form.find('[name="block-height"]').val();
		formData.blockWidth= form.find('[name="block-width"]').val();
		formData.tizerVert= form.find('[name="tizer-vert"]').val();
		formData.tizerGoriz= form.find('[name="tizer-goriz"]').val();
		formData.blockBorder= form.find('[name="block-border"]:checked').val();
		formData.borderKind= form.find('[name="border-kind"]:checked').val();
		formData.borderColor= form.find('[name="border-color"]').minicolors('rgbaString') + ';';
		formData.borderThickness= form.find('[name="border-thickness"]').val();
		formData.imgSideText= form.find('[name="img-side-text"]:checked').val();
		formData.bgColor= form.find('[name="bg-color"]').minicolors('rgbaString') + ';';
		// formData.tizerValue= form.find('[name="tizer-value"]').val(); удалено
		formData.tizerHeight= form.find('[name="tizer-height"]').val();
		formData.tizerWidth= form.find('[name="tizer-width"]').val();
		formData.textSize= form.find('[name="text-size"]').val();
		formData.showHideLink= form.find('[name="show-hide-link"]:checked').val();
		formData.textLink= form.find('[name="link-text"]').val();
		formData.colorLink= form.find('[name="link-color"]').minicolors('rgbaString') + ';';
		formData.sizeLink= form.find('[name="link-size"]').val();
		formData.tizerProportions= form.find('[name="proportions"]:checked').val();
		formData.textAligment= form.find('[name="text-aligment"]:checked').val();




		console.log('formData =', formData);

		createDemoData();
	}

	function createDemoData() {
		// console.log('createDemoData');
		// reset styles and values to default
		var currentDemoData = JSON.parse(JSON.stringify(defaultDemoData));
		// extend current styles and values from form
		currentDemoData.title.txt = formData.blockTitle;
		// console.log(formData.imgSideText);

		switch(formData.imgSideText) {
			case 'img-down':
				currentDemoData.wrapper['flex-direction'] = 'column';
				currentDemoData.wrapper['align-items'] = 'center';
				currentDemoData.imageWrap.order = 2;
				currentDemoData.caption.order = 1;
				currentDemoData.caption['padding-bottom'] = '15px';
				currentDemoData.logo['right'] = '0';
				currentDemoData.logo['left'] = '0';
				currentDemoData.logo['margin-left'] = 'auto';
				currentDemoData.logo['margin-right'] = 'auto';
				break;
			case 'img-up':
				currentDemoData.wrapper['flex-direction'] = 'column';
				currentDemoData.wrapper['align-items'] = 'center';
				currentDemoData.imageWrap.order = 1;
				currentDemoData.caption.order = 2;
				currentDemoData.caption['padding-top'] = '15px';
				currentDemoData.logo['right'] = '0';
				currentDemoData.logo['left'] = '0';
				currentDemoData.logo['margin-left'] = 'auto';
				currentDemoData.logo['margin-right'] = 'auto';
				break;
			case 'img-right':
				currentDemoData.imageWrap.order = 2;
				currentDemoData.caption.order = 1;
				currentDemoData.caption['padding-right'] = '13px';
				currentDemoData.wrapper['margin-right'] = '15px';
				currentDemoData.logo['right'] = 'auto';
				currentDemoData.logo['left'] = '4px';
				currentDemoData.logo['margin-left'] = '0';
				currentDemoData.logo['margin-right'] = '0';
				break;
			default:
				currentDemoData.imageWrap.order = 1;
				currentDemoData.caption.order = 2;
				currentDemoData.caption['padding-left'] = '13px;';
				currentDemoData.wrapper['margin-right'] = '15px';
				currentDemoData.caption['padding-top'] = '3px';
				currentDemoData.logo['right'] = '4px';
				currentDemoData.logo['left'] = 'auto';
				currentDemoData.logo['margin-left'] = '0';
				currentDemoData.logo['margin-right'] = '0';

		}
		currentDemoData.imageWrap.width = formData.tizerWidth;
		currentDemoData.imageWrap.height = formData.tizerHeight;
		currentDemoData.img.width = formData.tizerWidth;
		currentDemoData.img.height = formData.tizerHeight;
		currentDemoData.showHideLink = formData.showHideLink;
		currentDemoData.tizerVert = formData.tizerVert;
		currentDemoData.tizerGoriz = formData.tizerGoriz;
		currentDemoData.wrapper.width = (100 / (formData.tizerGoriz ? formData.tizerGoriz : 1)) + '%;';
		currentDemoData.tizerValue = formData.tizerValue;
		currentDemoData.textSize = formData.textSize;
		currentDemoData.textLink = formData.textLink;
		currentDemoData.sizeLink = formData.sizeLink;
		currentDemoData.colorLink = formData.colorLink;
		currentDemoData.bgColor = formData.bgColor;
		currentDemoData.borderKind = formData.borderKind;
		currentDemoData.borderThickness = formData.borderThickness;
		currentDemoData.borderColor = formData.borderColor;
		currentDemoData.blockBorder = formData.blockBorder;
		currentDemoData.blockHeight = formData.blockHeight;
		currentDemoData.blockWidth = formData.blockWidth;
		currentDemoData.blockValue = formData.blockValue;
		currentDemoData.textAligment = formData.textAligment
		acceptDemoData(currentDemoData);
	}

	function acceptDemoData(demoData) {
		// console.log('demoData =', demoData);
		resetDemoTemplate();
		// bocksRowWrapper 
		var totalWrap = document.createElement('div');
		totalWrap.className = 'blocks-wrapper';
		totalWrapStyle = '';

		totalWrapStyle += ' height: ' + demoData.blockHeight + demoData.blockValue + ';';
		totalWrapStyle += ' width: ' + demoData.blockWidth + demoData.blockValue + ';';
		totalWrapStyle += ' padding: 15px;';
		totalWrapStyle += ' background: ' + demoData.bgColor;
		totalWrapStyle += ' overflow: hidden;';
		totalWrapStyle += ' position: relative;';
		if (demoData.blockBorder !== 'hideborder') {
			totalWrapStyle += ' border-style: ' + demoData.borderKind + ';';
			totalWrapStyle += ' border-width: ' + demoData.borderThickness + 'px;';
			totalWrapStyle += ' border-color: ' + demoData.borderColor;
		}
		if (demoData.blockValue == 'px') {
			$('.after-px-block').text('px');
		}
		if (demoData.blockValue == '%') {
			$('.after-px-block').text('%');
		}
		totalWrap.setAttribute('style', totalWrapStyle);

		for (var rowIindex = 0; rowIindex < demoData.tizerVert; rowIindex++) {
			// bocksRow 
			var bocksRow = document.createElement('div');
			var bocksRowStyle = '';
			bocksRowStyle += displayFlex;
			bocksRowStyle += ' overflow: hidden;';
			if (rowIindex < demoData.tizerVert - 1) {
				bocksRowStyle += ' margin-bottom: 15px';
			}
			bocksRow.setAttribute('style', bocksRowStyle);
			for (var index = 0; index < demoData.tizerGoriz * 1; index++) {
				bocksRow.appendChild(createBlock(demoData));
				// totalWrap.appendChild(createBlock(demoData.placeLogo));
			}
			totalWrap.appendChild(bocksRow);
			
		}
		
		var logoImg = document.createElement('img');
		var logoImgStyle = '';
		logoImgStyle += 'display: block; ';
		logoImgStyle += 'position: absolute; ';
		logoImgStyle += 'bottom: 3px; ';
		logoImgStyle += 'left: ' + demoData.logo.left + '; ';
		logoImgStyle += 'right: ' + demoData.logo.right + '; ';
		logoImgStyle += 'margin-right: ' + demoData.logo['margin-right'] + '; ';
		logoImgStyle += 'margin-left: ' + demoData.logo['margin-left'] + '; ';
		logoImg.setAttribute('src', demoData.logo.src);
		logoImg.setAttribute('alt', demoData.logo.alt);
		logoImg.setAttribute('style', logoImgStyle);
		totalWrap.appendChild(logoImg);
		demoContentWrap.append($(totalWrap));
	}

	function resetDemoTemplate() {
		// clear preview wrapper
		demoContentWrap.empty();
	}

	function createBlock(demoData) {
		// create elements
		var wrap = document.createElement('div');
		wrap.className = 'embed__place-wrap';
		var wrapStyle = '';
		wrapStyle += ' width: ' + demoData.wrapper.width;
		wrapStyle += displayFlex;
		wrapStyle += setFlexShrink(0);
		// wrapStyle += ' min-width: 318px;';
		if (demoData.wrapper['flex-direction']) {
			wrapStyle += flexDirectionColumn;
		}
		if (demoData.wrapper['margin-right']) {
			wrapStyle += ' margin-right:' +  demoData.wrapper['margin-right'];
		}
		if (demoData.wrapper['margin-left']) {
			wrapStyle += ' margin-left:' +  demoData.wrapper['margin-left'];
		}
		if (demoData.wrapper['align-items']) {
			switch(demoData.wrapper['align-items']) {
				case 'center':
					wrapStyle += alignItemsCenter;
					break;
				case 'baseline':
					wrapStyle += alignItemsBaseline;
					break;
				case 'flex-end':
					wrapStyle += alignItemsFlexEnd;
					break;
				default:
					wrapStyle += alignItemsFlexStart;
			}
		}
		
		wrap.setAttribute('style', wrapStyle);

		
		// placeImage 
		var placeImage = document.createElement('div');
		placeImage.className = 'embed__place-image';
		var placeImageStyle = '';
		placeImageStyle += ' width: ' + demoData.imageWrap.width + 'px;';
		placeImageStyle += ' height: ' + demoData.imageWrap.height + 'px;';
		placeImageStyle += setFlexOrder(demoData.imageWrap.order);
		placeImageStyle += flexShrink ;
		
		placeImage.setAttribute('style', placeImageStyle);

		
		// imageStyle 
		var image = document.createElement('img');
		var imageStyle = '';
		imageStyle += ' width: 100%;';
		imageStyle += ' height: 100%;';
		image.setAttribute('src', demoData.img.src);
		image.setAttribute('alt', '');
		image.setAttribute('style', imageStyle);


		// logoStyle 
		var logo = document.createElement('img');
		var logoStyle = '';
		logoStyle += ' width: 100%;';
		logoStyle += ' height: 100%;';
		logo.setAttribute('src', demoData.logo.src);
		logo.setAttribute('alt', '');
		logo.setAttribute('style', imageStyle);

		// placeLogo
		var placeLogo = document.createElement('div');
		placeLogo.className = 'embed__place-logo';
		var placeLogoStyle = '';
		placeLogoStyle += ' width: ' + '100' + 'px;';
		placeLogoStyle += ' height: ' + '100' + 'px;';
		placeLogoStyle += setFlexOrder(demoData.imageWrap.order);
		placeLogoStyle += flexShrink ;
		
		placeLogo.setAttribute('style', placeLogoStyle);

		
		// placeCaption 
		var placeCaption = document.createElement('div');
		placeCaption.className = 'embed__place-caption';
		var placeCaptionStyle = '';
		
		placeCaptionStyle += ' text-align: ' + demoData.textAligment + ';';

		
		placeCaptionStyle += setFlexOrder(demoData.caption.order) + ';';
		if (demoData.caption['padding-right']) {
			placeCaptionStyle += ' padding-right: ' + demoData.caption['padding-right'];
		}
		if (demoData.caption['padding-left']) {
			placeCaptionStyle += ' padding-left: ' + demoData.caption['padding-left'];
		}
		if (demoData.caption['padding-bottom']) {
			placeCaptionStyle += ' padding-bottom: ' + demoData.caption['padding-bottom'] + ';';
		}
		if (demoData.caption['padding-top']) {
			placeCaptionStyle += ' padding-top: ' + demoData.caption['padding-top'] + ';';
		}
		if (demoData.caption['text-align']) {
			placeCaptionStyle += ' text-align: ' + demoData.caption['text-align'] + ';';
		}
		
		
		placeCaption.setAttribute('style', placeCaptionStyle);

		
		// placeTitle 
		var placeTitle = document.createElement('div');
		placeTitle.className = 'embed__place-title';



		// placeTitleSpan 
		var placeTitleSpan = document.createElement('span');
		placeTitleSpan.innerHTML = demoData.title.txt;
		var placeTitleSpanStyle = '';
		placeTitleSpanStyle += ' font-size: ' + demoData.textSize + 'px' + ';';
		placeTitleSpanStyle += ' font-family: Ubuntu ' + ';';
		placeTitleSpanStyle += ' letter-spacing: normal; ';
		placeTitleSpanStyle +=  displayFlex + ';';

		placeTitleSpan.setAttribute('style', placeTitleSpanStyle);

		
		// placeLink
		var placeLink = document.createElement('a');
		var placeLinkStyle = '';
		placeLinkStyle += ' font-size: ' + demoData.sizeLink  + 'px;';
		placeLinkStyle += ' font-family: Ubuntu;';
		placeLinkStyle += ' color: ' + demoData.colorLink;
		placeLink.className = 'embed__place-link';
		placeLink.setAttribute('href', '#');
		placeLink.innerHTML = demoData.textLink;
		

		placeLink.setAttribute('style', placeLinkStyle);

		// build block
		placeImage.appendChild(image);
		placeTitle.appendChild(placeTitleSpan);
		placeCaption.appendChild(placeTitle);
		placeLogo.appendChild(logo);
		if (demoData.showHideLink !== 'hide-link') {
			placeCaption.appendChild(placeLink);
		}
		wrap.appendChild(placeImage);
		wrap.appendChild(placeCaption);

		return wrap;
	}

	function setFlexOrder(val) {
		return ' -webkit-box-ordinal-group: ' + (val + 1) + '; -ms-flex-order: ' + val + '; order: ' + val + ';';
	}

	function setFlexShrink(val) {
		return ' -ms-flex-negative: ' + val + '; flex-shrink: ' + val + ';';
	}


	$('.minicolors').minicolors();

	$('.minicolors-btn').click(function() {
		$('.minicolors-panel').addClass("hide-color");
	});
	$('.minicolors-input , .minicolors-swatch ').click(function() {
		$('.minicolors-panel').removeClass("hide-color");
	});

};




$('.c-header__form--cansel').on('click', function (e) {
	$('.mobile-menu__search').removeClass('active');
	$('.c-header__form--cansel').removeClass('active');
	setTimeout(function(){ 
		$('.c-header__form--autocomplit').removeClass('active');
	 }, 0);
	$('.js-search').val('');
})






	$(function() {
		var availableTutorials  =  [
			 "apple.com",
			 "techcrunch.com",
			 "forbes.ru",
			 "google.com",
			 "mazda.com/ru",
			 "samsung.com",
		];
		$("#search_by_domain").autocomplete({
			 source: availableTutorials
		}),
		$("#domen_place2").autocomplete({
			 source: availableTutorials
		});
 });


 $(".is-input").find('input').attr("placeholder", "Выберите или введите текст");