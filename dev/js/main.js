/* frameworks */
//=include ../../dist/bower_components/moff/dist/moff.min.js
//=include ../../dist/bower_components/bootstrap/js/dist/tooltip.js
//=include ../../dist/bower_components/bootstrap/js/dist/collapse.js
//=include ../../dist/bower_components/bootstrap/js/dist/util.js
//=include ../../dist/bower_components/bootstrap/js/dist/modal.js
//=include ../../dist/bower_components/bootstrap/js/dist/tab.js
//=include ../../dist/bower_components/bootstrap/js/dist/collapse.js
//=include ../../dist/bower_components/jquery-validation/dist/jquery.validate.min.js
//=include ../../dist/bower_components/svg4everybody/dist/svg4everybody.min.js

/* libs */
//=include lib/modernizr-custom.js

/* plugins */

/* separate */
//=include helpers/object-fit.js
//=include plugins/picker.js
//=include plugins/datepicker/moment.min.js
//=include plugins/datepicker/daterangepicker.js
//=include separate/global.js

/* components */
// components/js-header.js
//=include components/js-select.js
//=include components/js-constructor.js
//=include components/clipboard.js

// the main code
svg4everybody()

window.moffConfig = {
	// Website CSS breakpoints.
	// Default values from Twitter Bootstrap.
	// No need to set xs, because of Mobile first approach.
	breakpoints: {
		sm: 768,
		md: 992,
		lg: 1200
	},
	loadOnHover: true,
	cacheLiveTime: 2000
};

// Moff.amd.register({
// 	id: 'header',
// 	// depend: {
// 	// 	js: ['http://chat-domain.com/js/chat-api.js'],
// 	// 	css: ['http://chat-domain.com/css/chat.css']
// 	// },
// 	file: {
// 		js: ['s/js/components/js-header.js']
// 	},

// 	beforeInclude: function() {},
//     afterInclude: function() {},
    
// 	loadOnScreen: ['md', 'lg'],
// 	onWindowLoad: true
// });

if($('.js-form-check').length > 0) {
	Moff.amd.register({
			id: 'validation',
			depend: {
					js: ['bower_components/jquery-validation/dist/jquery.validate.min.js']
			},
			file: {
					js: ['s/js/helpers/valid.js']
			},

			beforeInclude: function() {},
			afterInclude: function() {},

			loadOnScreen: ['xs', 'sm', 'md', 'lg'],
			onWindowLoad: true
	});
}


  function fixMenu() {
    var $menu = $('.c-menu');
    if ($(window).scrollTop() > 56)
			$($menu).addClass( "fixed-menu" );
			else
			$($menu).removeClass( "fixed-menu" );
		}
  $(window).scroll(fixMenu);
	fixMenu();
	

  function fixfilter() {
    var $menu = $('.c-table__filter-inner');
    if ($(window).scrollTop() > 240)
			$($menu).addClass( "fixed-filter" );
			else
			$($menu).removeClass( "fixed-filter" );
		}
  $(window).scroll(fixfilter);
	fixfilter();
	

	$('[data-toggle="tooltip"]:not(.tooltip-wide-trigger)').tooltip();
	$('.tooltip-wide-trigger[data-toggle="tooltip"]').tooltip({
		template: '<div class="tooltip tooltip-wide" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
	});


	$('.collapse').collapse()



	$('.c-table__item--content').on('click', function(e){
		$(this).closest('.c-table__item-wrap').toggleClass('active-no-block');
	});
	$('body').on('click', function(e){
		if($(e.target).closest('.c-table__item-wrap').length === 0) {
			$('.c-table__item-wrap').removeClass('active-no-block');
		}
	});


	
	$(document).ready(function(){
		$('body').append('<a href="#" id="scroller" title="Вверх">Вверх</a>');
	});

	
	$(function() {
	 $.fn.scrollToTop = function() {
		$(this).hide().removeAttr("href");
		if ($(window).scrollTop() >= "250") $(this).fadeIn("slow")
		var scrollDiv = $(this);
		$(window).scroll(function() {
		 if ($(window).scrollTop() <= "250") $(scrollDiv).fadeOut("slow")
		 else $(scrollDiv).fadeIn("slow")
		});
		$(this).click(function() {
		 $("html, body").animate({scrollTop: 0}, "slow")
		})
	 }
	});



	var clipboard = new Clipboard('.copy-letter-button');


	$('.hide-graph').on('click', function(){
		$('.show-graph').show();
		$('.c-stats__graph').hide();
	});
	$('.show-graph').on('click', function(){
		$('.show-graph').hide();
		$('.c-stats__graph').show();
	});

	$('.c-finances__withdrawal-actions-btn button').click(function() {
    $(this).css('background-color', '#002750');
})




	$('input[name="datepicker"]').daterangepicker({
		"autoApply": true,
		// singleDatePicker: true,
    "locale": {
        "format": "MM.DD.YYYY",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "weekLabel": "W",
        "daysOfWeek": [
						"В",
            "П",
            "В",
            "С",
            "Ч",
            "П",
						"С"
        ],
        "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
        "firstDay": 1
		}
		
}
);




if($('input[name="auto-withdrawal"]').is(':checked'))
{
  
}else
{
	$( ".c-finances__auto-withdrawal-sum" ).hide();
}




$('.c-chat__small').on('click', function(){
  $( ".c-chat__big" ).show();
  $( this ).hide();
});

$('.js-all-contacts').on('click', function(){
  $( ".c-chat__big" ).hide();
  $( ".c-chat__middle" ).show();
});

$('.js-close-middle').on('click', function(){
  $( ".c-chat__middle" ).hide();
  $( ".c-chat__big" ).show();
});
$('.c-chat__middle-head-close').on('click', function(){
  $( ".c-chat__middle" ).hide();
  $( ".c-chat__big" ).hide();
  $( ".c-chat__small" ).show();
});